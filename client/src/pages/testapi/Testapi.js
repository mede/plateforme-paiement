import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

function Testapi(props) {

    const [products, setProduct] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const {data} = await axios.get("/api/products")
            setProduct(data)
        }
        fetchData();
        return () => {
            //
        }
    }, [])

  return (
      <div>
          <ul>
            {
              products.map(product => 
                <li>
                    <Link to = {'/shop/'}>
                    </Link>
                </li>)
            }
          </ul>
        </div>
  );
}
export default Testapi;
