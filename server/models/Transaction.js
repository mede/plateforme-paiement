// const db = require('../lib/db');
const mongoose = require("mongoose");
const transactionSchema = new mongoose.Schema(
  {
    total: String,
    status: String,
    panier: Array
  },

  {collection: "Transactions"}
);

const Transaction = mongoose.model("Transactions", transactionSchema);

module.exports = Transaction;