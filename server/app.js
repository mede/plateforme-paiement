const express = require("express");
const PORT = process.env.PORT || 5000;
const bodyParser = require('body-parser');
const logger = require('morgan');
// const movieRouter = require("./routes/movies");
const securityRouter = require("./routes/security");
const paymentRouter = require("./routes/payment");
const usersRoutes = require("./routes/users");
// const verifyToken = require("./middlewares/verifyToken");
// const mustacheExpress = require("mustache-express");
const transactionRoutes = require("./routes/transactions");

const app = express();
// app.engine("mustache", mustacheExpress());
// app.set("view engine", "mustache");
// app.set("views", __dirname + "/views");

app.use(express.static(`${__dirname}/client/build`))
app.use(logger('dev'));
app.use(express.json());
// app.use(express.urlencoded());

app.get("/api/products", (req, res) => {
  res.json({ msg: "Hello la terre" });
});

app.use(paymentRouter);
app.use(securityRouter);
// app.use(verifyToken);
app.use(bodyParser.json());

app.get('/api', (req, res) => {
	res.json({message: "API root."})
})
app.use(transactionRoutes);
app.use("/api/transactions", transactionRoutes);


app.use("/api/users", usersRoutes);

app.use('*', (req, res) => {
	res.sendFile(`${__dirname}/client/build/index.html`)
})

app.listen(PORT, (err) => {
	console.log(err || `Server running on port ${PORT}.`)
})